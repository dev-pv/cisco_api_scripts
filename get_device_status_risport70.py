import smtplib
from zeep import Client, Settings
from zeep.cache import SqliteCache
from zeep.transports import Transport
import requests
from requests import Session
from requests.auth import HTTPBasicAuth
from zeep.exceptions import Fault
import urllib3
import sys

 
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
 
username = ''
password = ''
server = '10.255.200.3'
wsdl = 'https://10.255.200.3:8443/realtimeservice2/services/RISService70?wsdl'


session = Session()
session.verify = False
session.auth = HTTPBasicAuth(username, password)
requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)
transport = Transport(cache=SqliteCache(), session=session, timeout=20)
settings = Settings( strict = False, xml_huge_tree = True )
client = Client(wsdl=wsdl, settings = settings, transport=transport)

service = client.create_service(
    '{http://schemas.cisco.com/ast/soap}RisBinding',
    f'https://{ server }:8443/realtimeservice2/services/RISService70' 
)

stateInfo = ''

DEVICE_NAME = ''

criteria = {  
    'MaxReturnedDevices': '1000',  
    'DeviceClass': 'Phone',  
    'Model': '255',  
    'Status': 'Any',  
    'NodeName': '',  
    'SelectBy': 'Name',  


    'Protocol': 'Any',  
    'DownloadStatus': 'Any', 
    'SelectItems': {
        'item': [ ]
    } 
}

criteria[ 'SelectItems' ][ 'item' ].append({'Item': DEVICE_NAME})

try:
    resp = service.selectCmDevice( stateInfo, criteria )
except Fault as err:
    print( f'Zeep error: selectCmDevice: { err }' )
    sys.exit( 1 )
resp = resp['SelectCmDeviceResult']['CmNodes']['item'][1]['CmDevices']['item']

# for i in resp:
#     if i['Status'] == 'Registered':
#         print('Device ' + i['Name'] + ' Status is ' + i['Status'])

total = []
for i in resp:
    temp = []
    temp.append(i['Name']) 
    temp.append(i['Status']) 
    temp = ' '.join(temp)
    total.append(temp)
