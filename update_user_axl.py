from zeep import Client
from zeep.cache import SqliteCache
from zeep.transports import Transport
from requests import Session
from requests.auth import HTTPBasicAuth
import urllib3
import xlrd

book = xlrd.open_workbook(r"C:\Users\user\Downloads\enduser_passwords.xls")
sheet = book.sheet_by_index(0)
col_enduser = sheet.col_values(0)
col_password = sheet.col_values(1)
 
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
 
username = ''
password = ''
cucm_url = 'https://10.255.200.3:8443/axl/'

wsdl = 'file://C:/Users/user/axl_cucm_py/axlsqltoolkit/schema/14.0/AXLAPI.wsdl'

session = Session()
session.verify = False
session.auth = HTTPBasicAuth(username, password)
transport = Transport(cache=SqliteCache(), session=session, timeout=20)
client = Client(wsdl=wsdl, transport=transport)
service = client.create_service("{http://www.cisco.com/AXLAPIService/}AXLAPIBinding", cucm_url)


for i, k in zip(col_enduser, col_password):
     service.updateUser(userid=i, password=k)


